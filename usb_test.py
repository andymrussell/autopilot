import sys
import usb.core

# Find a USB device
dev = usb.core.find(find_all=True)

# Loop through devices, printing vendor and product IDs in decimal and hex
for cfg in dev:
	sys.stdout.write('Decimal VendorID=' + str(cfg.idVendor) + ' and ProductID=' + str(cfg.idProduct) + '\n')
	sys.stdout.write('Hexadecimal VendorID=' + hex(cfg.idVendor) + ' and ProductID=' + hex(cfg.idProduct) + '\n\n')
