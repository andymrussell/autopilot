import json
from threading import Thread
from time import sleep

import hid.event_manager
import hid.button.driver
import hid.display.gui

class Autopilot:
	settings = {}
	
	def __init__(self):
		self.load_settings()
		
	def start(self):
		# Start the event host
		print("Starting event backend")
		eventhost = Thread(target = hid.event_manager.event_manager)
		eventhost.start()
		
		displaydriver = Thread(target = hid.display.gui.gui)
		displaydriver.start()
		
		# Start the button drivers
		btn = {}
		for btn_label,btn_id in self.settings["button"].iteritems():
			print("Starting button " + btn_label + " at " + btn_id)
			btn[btn_label] = Thread(target = hid.button.driver.button, kwargs = {"pin":btn_id})
			btn[btn_label].start()
		
		print("Beginning wait loop")
		while eventhost.is_alive():
			sleep(1)
	
	def load_settings(self, file = "autopilot.conf"):
		f = open(file)
		s = f.read()
		f.close()
		self.settings = json.loads(s)

	def save_settings(self, file = "autopilot.conf"):
		s = json.dumps(self.settings)
		f = open(file)
		f.write(s)
		f.close()

if __name__ == "__main__":
	ap = Autopilot()
	ap.start()