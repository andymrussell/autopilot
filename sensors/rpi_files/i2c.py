import smbus
import time

isquc = smbus.SMBus(1)
address = 0x1e

out_file = open("i2coutput.txt","w")
for i in range(0,50):
    try:
        byte_reading = isquc.read_byte(address)
        #print byte_reading,"{0:b}".format(byte_reading)
        
        if byte_reading!=0:
            out_file.write("dec: "+str(byte_reading)+" bin: "+str("{0:b}".format(byte_reading)+"\n"))
            
        print "\nread_byte: "+str(byte_reading)+" or "+str("{0:b}".format(byte_reading))
        
        print "read_byte_data at locations\n0  1  2 3 4  5 = "
        #for index in range(0,5):
        a = isquc.read_byte_data(address,0)
        b = isquc.read_byte_data(address,1)
        c = isquc.read_byte_data(address,2)
        d = isquc.read_byte_data(address,3)
        e = isquc.read_byte_data(address,4)
        f = isquc.read_byte_data(address,5)
        print a,b,c,d,e,f
    except IOError, err:
        print err
    #print str("{0:b}".format(isquc.read_byte_data(address,5)))
    #time.sleep(0.5)
#out_file.close()
#out_file = open("i2coutput.txt","r")
#print out_file.read()
out_file.close()
