# Travis Sandidge
# This file is to interface with the gps via UART on the RPi, eventually on the BB (Beaglebone Black)
# This is a modular file, to be used in conjunction with the central GPS script
# Could possibly be turned into a class/function at some point

#!!!!! Not a working script right now. 20140126-0030

import serial,time
uartport = '/dev/ttyAMA0'

uart = serial.Serial(uartport, 9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=3)
#uart.write("Raspberry pi\r\n")
#uart.timeout = 2
#print ("write data = Raspberry pi")
print ("Time to play.\n")
loop  = True
count = 0
print "Begin loop:\n"
while (loop):
    #response=uart.read(uart.inWaiting())
	#time.sleep(2)
	#print (" read data: pithon \n")
	#print (response)
    #try:
    state = uart.read(1)
    state_read = uart.readline(None)
    print "read: [  "+state+"  ]; [  "+state_read+"  ] :readline"
    print "CTS: "+ str(uart.getCTS())+" --- DSR: "+str(uart.getDSR())+" --- RI: "+str(uart.getRI())+" --- CD: "+str(uart.getCD())+"\n"
    #except:
    #    pass
    print "Try "+str(count+1)+".\n"
    count += 1
    if count == 3:
        loop = False
uart.close()