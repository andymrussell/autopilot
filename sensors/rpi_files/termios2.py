import os, termios, inspect, serial
import time

uartport = '/dev/ttyAMA0'
serialfd = ''

def lineno():
    #Returns the current line number in our program.
    return inspect.currentframe().f_back.f_lineno

def checkIfConsIsSet(stats, const, cname):
    if stats & const:
        print '%s is set' % cname
        return True
    else:
        print '%s is NOT set' % cname
        return False
# unused shtuff: os.O_NONBLOCK
try:
    print "Openning serial port..."
    serialfd = os.open(uartport, os.O_RDWR)
"""    print lineno()
    get_attr = termios.tcgetattr(serialfd)
    print "serial attr: "+get_attr
    print lineno()
    if serialfd:
        print lineno()
        print "Serial port settings: ", termios.tcgetattr(serialfd)
#        print "Constants: MS_DCD=%d, MS_RTS=%d" % (termios.MS_DCD, termios.MS_RTS)
#        # Getting serial stats
#        estat, mstat = termios.tcgetstats(serialfd)
#        print "1 termios stats: ", (estat, mstat)
#        checkIfConsIsSet(mstat, termios.MS_RTS, 'MS_RTS')
#        print '>>> Toggling MS_RTS'
#        termios.tcsetsignals(serialfd, termios.MS_RTS)
#        estat, mstat = termios.tcgetstats(serialfd)
#        print "2. termios stats, mstat: ", (estat, mstat)
#        checkIfConsIsSet(mstat, termios.MS_RTS, 'MS_RTS')
#        print 'Waiting 5 secs...'
#        time.sleep(5)
#        print '>>> Reseting the MS_RTS...'
#        tmp = mstat & ( not termios.MS_RTS )
#        termios.tcsetsignals(serialfd, tmp)
#        estat, mstat = termios.tcgetstats(serialfd)
#        print "3. termios stats: ", (estat, mstat)
#        checkIfConsIsSet(mstat, termios.MS_RTS, 'MS_RTS')
#        print "Closing serial port..."
    os.close(serialfd)
    """
except:
    print "\n"+str(lineno())
    print "Error: Problem occured while openning socket"
