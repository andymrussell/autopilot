import zmq
import json

ctx = zmq.Context()
txgps = ctx.socket(zmq.PUB)
txgps.connect("tcp://192.168.1.30:12000")
rxgps = ctx.socket(zmq.SUB)
rxgps.connect("tcp://192.168.1.30:12001")
rxgps.setsockopt(zmq.SUBSCRIBE, "gps pos")

while True:
    data = rxgps.recv().split(" ",2)
    data = json.loads(data[2])
    magdiff = float(data["magdiff"][:-1]) if data["magdiff"][-1] == "E" else -float(data["magdiff"][:-1])
    maghdg = round((data["hdg"] - magdiff) % 360,1)
    txgps.send("hdg virt " + json.dumps({"true":data["hdg"], "mag":maghdg}))
