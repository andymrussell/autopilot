import SocketServer
import pynmea2
import zmq
import json

ctx = zmq.Context()
txgps = ctx.socket(zmq.PUB)
txgps.connect("tcp://192.168.1.30:12000")

class GPSHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip().splitlines(False)
        for sentence in data:
            txgps.send("log gps fg " + sentence)
            
            if sentence.startswith("$GPRMC"):
                nmea = pynmea2.parse(sentence)
                
                lat = float(nmea.lat[:nmea.lat.find(".")-2]) + float(nmea.lat[nmea.lat.find(".")-2:])/60
                lon = float(nmea.lon[:nmea.lon.find(".")-2]) + float(nmea.lon[nmea.lon.find(".")-2:])/60
                
                pack = {"lat": ("%.6f" % lat) + nmea.lat_dir,
                        "lon": ("%.6f" % lon) + nmea.lon_dir,
                        "gs": nmea.spd_over_grnd,
                        "hdg": nmea.true_course,
                        "magdiff": nmea.mag_variation + nmea.mag_var_dir}
                if nmea.data_validity != "A":
                    pack["warn"] = "Navigation receiver warning"
                txgps.send("gps pos " + json.dumps(pack))
                
            elif sentence.startswith("$GPGGA"):
                nmea = pynmea2.parse(sentence)
                
                pack = {"time": nmea.timestamp.isoformat(),
                        "sig_qual": nmea.gps_qual,
                        "num_sats": nmea.num_sats,
                        "alt": nmea.altitude,
                        "diff_age": nmea.age_gps_data}
                txgps.send("gps info " + json.dumps(pack))

if __name__ == "__main__":
    HOST, PORT = "127.0.0.1", 12100
    
    server = SocketServer.UDPServer((HOST, PORT), GPSHandler)
    server.serve_forever()
