import SocketServer
import zmq
import json

ctx = zmq.Context()
tximu = ctx.socket(zmq.PUB)
tximu.connect("tcp://192.168.1.30:12000")

class IMUHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = self.request[0].strip().split('\t')
        data = {"p":data[1],
                "q":data[2],
                "r":data[3],
                "ax":data[4],
                "ay":data[5],
                "az":data[6]}
        tximu.send("imu fg " + json.dumps(data))

if __name__ == "__main__":
    HOST, PORT = "127.0.0.1", 12101
    
    server = SocketServer.UDPServer((HOST, PORT), IMUHandler)
    server.serve_forever()
