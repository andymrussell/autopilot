import Adafruit_BBIO.UART as UART
import sys,signal,serial,inspect,os,time,zmq,pynmea2,SocketServer, json
import smtplib

# Ima send an email to myself
fromaddr = 'at.autopilot@gmail.com'
toaddrs = {'tsandidge@live.com','andymrussell777@gmail.com'}
msg = 'GPS mail server working. '

# Credentials
username = 'at.autopilot'
passwd = 'gps ap sp 2014'

# Start the server; borrowed from Mark Sanborn of nixtutor.com
#mailserver = smtplib.SMTP('smtp.gmail.com:587')
#mailserver.starttls()
#mailserver.login(username,passwd)
#msg = msg + str(time.ctime())
#mailserver.sendmail(fromaddr,toaddrs,msg)


# Make sure that the driver is 'installed'
#if os.system("cat /sys/devices/bone_capemgr.9/slots | grep 'ADAFRUIT-UART1'") is None:
os.system("echo ADAFRUIT-UART1 > /sys/devices/bone_capemgr.9/slots")

# Setup zmq client
context = zmq.Context()
tx_gps = context.socket(zmq.PUB)
tx_gps.connect("tcp://127.0.0.1:12000")

# Open up the serial port to a variable, ser
ser = serial.Serial(port = "/dev/ttyO1", baudrate = 9600, timeout=3) # timeout=0 is non-blocking mode (asynchronous I/O)

def signal_handler(signal, frame): # Capture things like SIGINT (Ctrl+c)
	print("Time to go!")
	mailserver.quit()
	ser.close()
	print("Kuht fa osun!")
	sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)
# This will return the current line number of your program file
def lineno():
	return inspect.currentframe().f_back.f_lineno

count = 0

def gpsPackager(raw_data,type):
	#context = zmq.Context()

	# Receive on
	#tx_gps = context.socket(zmq.PUB)
	#tx_gps.connect("tcp://172.20.16.153:12000")
	
	if raw_data is '0':
		tx_gps.send("log gps bb | Err: Nothing to send")
	else:
		tx_gps.send("log gps bb " + raw_data)
	
	if raw_data.startswith("$GPRMC"): # Recommended minimum specifications for GPS/transit data
#		print str(lineno()) + ": %s section." % str(type)
		nmea = pynmea2.parse(raw_data)
		
		if nmea.data_validity != "A":                                                                	
    		# These packets contain a status field, where A indicates 'Active' and V indicates 'Void'

			pack = {"lat"		: "0",
					"lon"		: "0",
					"gs" 		: "0",
					"hdg"		: "0",
					"magdiff"	: "0"}
			pack["warn"] = "Navigation receiver warning"
	
		else:
			#print str(lineno()) +  ": latitude = " + nmea.lat + " and longitude = " + nmea.lon
			print "We had a lock on %s " % str(time.ctime())
			if count == 0:
				mailserver.login(username,passwd) # May need to move this eventually, or not use it at all. Used for 'debugging'
				msg = "GPS has finally had a lock at %s." % str(time.ctime())
				lon = float(nmea.lon[:nmea.lon.find(".")-2]) + float(nmea.lon[nmea.lon.find(".")-2:])/60

			pack = {"lat"		: ("%.6f" % lat) + nmea.lat_dir,
					"lon"		: ("%.6f" % lon) + nmea.lon_dir,
					"gs" 		: nmea.spd_over_grnd,
					"hdg"		: nmea.true_course,
					"magdiff"	: nmea.mag_variation + nmea.mag_var_dir}
			
		tx_gps.send("gps pos " + json.dumps(pack))
		#print str(lineno()) +  ": json pack is " + str(pack)
	
	elif raw_data.startswith("$GPGGA"):
		nmea = pynmea2.parse(raw_data)
#		print str(lineno()) + ": %s section at %s." % (str(type), str(time.ctime()))	
	
		pack = {"time"		: nmea.timestamp,#.isoformat(),# When empty, can't conver Nonetype to isoformat!
				"sig_qual"	: nmea.gps_qual,
				"num_sats"	: nmea.num_sats,
				"alt"		: nmea.altitude,
				"diff_age"	: nmea.age_gps_data}
		
		tx_gps.send("gps info " + json.dumps(pack))

		#print str(lineno()) + ": json pack is " + str(pack)
	elif raw_data.startswith("$GPGSA"):
#		print str(lineno()) + ": %s section." % str(type) 
		nmea = pynmea2.parse(raw_data)
		
		pack = {"mode_fix_type"	: nmea.mode_fix_type # 1=no fix, 2=2D, 3=3D
				}
		
		tx_gps.send("gps fix " + json.dumps(pack))

#	try:
#		os.system("echo ADAFRUIT-UART1 > /sys/devices/bone_capemgr.9/slots")
#		ser = serial.Serial(port = "/dev/ttyO1", baudrate = 115200, timeout=10)
#		if ser.isOpen():
#    	    print "UART1 is up and ready! Woot!"
#		else:                                   	
#			print "Serial is not open!"         	
#			sys.exit(1)                         	
#		while True:
#			line_read = ser.readline()
#			gpsPackager(line_read)
#	except:
#		pass
#	finally:
#		print "Cleaning up..."
#		ser.close()
#		print "Kut fa osun!"
#		exit()

if ser.isOpen():
    print "UART1 is up and ready! Woot!"
else:
	print "Serial is not open!"
	sys.exit(1)

while True: # Let it rain
	print str(lineno())+": Are we here?"
	try:
		line_read = ser.readline()
		print line_read
		time.sleep(1)
	except:
		print str(lineno()) + ": Serial port ERR; no data to receive."
	gpsPackager(line_read,line_read[:6])

#if __name__ == "__main__":
#	line_read = ser.readline()
#	gpsPackager(line_read)
