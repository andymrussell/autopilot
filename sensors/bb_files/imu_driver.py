from Adafruit_I2C import Adafruit_I2C
import sys, signal, os, inspect
import json, time, math, smbus, zmq

MS_ADDR = 0x77 # Default MS5611 (altimiter) address (76, non-standard)
MPU_ADDR = 0x68 # Default MPU6050 (Accel & gyro) address (69, non-standard)
#address = 0x1E # HMC5883L (magnetometer)
HMC_ADDR = 0x1E
# Register addresses
MPU6050_PWR_MGNT_1 = 0x6B
# Defined Bits
SLEEP_BIT = 6
XGYRO_BIT = 0
try:
	i2c = Adafruit_I2C(MPU_ADDR) # Andy said to try nicer libraries... sorry, smbus.SMBus(1)
except IOError, err:
	print str(lineno()) + ": "+str(err)

def writeBit(addr, index, value): # This function will be used to write singular bits to addresses (effectively)
	curr_byte = bin(int(str(i2c.readU8(addr)), 10))[2:].zfill(8)
	mask = 1 << index
	
	if value == 0:
		new_byte = int(curr_byte) & ~(mask)
	
	elif value == 1:
		print str(lineno())+' '+curr_byte
		new_byte = int(curr_byte) | mask
	
	else:
		print "Err %s: Bit value must be 1 or 0!" % str(lineno())
	i2c.write8(addr, new_byte)

# TODO: Create a bitCheck function to use instead of if statements. Much more basic, and useful
def initSeq(): # This is essentially a port over from the FreeIMU code;
	# Most of this is calling from ~/libraries/MPU60X0.cpp
	pwr_mgnt_status = bin(int(str(i2c.readU8(0x6B)), 10))[2:].zfill(8) # This takes data from register 0x6B (base 10), and converts it to binary
	
	if str(pwr_mgnt_status).endswith('0'): # TODO: This should check all three bits, because we want only the one on.
		writeBit(MPU6050_PWR_MGNT_1,XGYRO_BIT,1)
		pwr_mgnt_status = bin(int(str(i2c.readU8(0x6B)), 10))[2:].zfill(8)
		print "Power management status after clock_sel: "+str(pwr_mgnt_status)
	
	else:
		print "Clock is set appropriately!"

	# Shut off sleep mode;
	pwr_mgnt_status = bin(int(str(i2c.readU8(0x6B)), 10))[2:].zfill(8) # This takes data from register 0x6B (base 10), and converts it to binary
	
	if str(pwr_mgnt_status).startswith("01"): # Checks to see if IMU (technically MPU6050) is awake
		writeBit(MPU6050_PWR_MGNT_1,SLEEP_BIT,0)
		pwr_mgnt_status = bin(int(str(i2c.readU8(0x6B)), 10))[2:].zfill(8) # This takes data from register 0x6B (base 10), and converts it to binary
		print "Power management status after wakeup: "+str(pwr_mgnt_status)
	
	else:
		print "We are awake!"

def signal_handler(signal, frame): # Capture things like SIGINT (Ctrl+c)
	print("Time to go!")
	print("Kuht fa osun!")
	sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# This will return the current line number of your program file
def lineno():
	return inspect.currentframe().f_back.f_lineno

def accelProcess(accel):
	# TODO:convert from 16-bit 2-compliment
	
	for i in range(0,len(accel)):
		accel[i] = bin(int(str(accel[i]), 10))[2:].zfill(8)	# This converts decimal numbers to binary
	
	print str(lineno()),accel
	
	accel = [i+j for i,j in zip(accel[::2],accel[1::2])]	# This concatenates every two list items (since each value is broken into two eights
	
	print str(lineno()),accel, type(accel)
	
	for i in range(0,len(accel)):
		
		if accel[i].startswith('1'):
			accel[i] = int(accel[i],2) ^ int('0xffff',16)
			print str(lineno()),accel[i],type(accel[i])
			accel[i] = -(accel[i]+1)
			print str(lineno()),accel[i],type(accel[i])
		
		else:
			accel[i] = int(accel[i],2)
			print str(lineno()),accel[i],type(accel[i])
	
	# TODO:publish to logger
	return accel[0],accel[1],accel[2]

def gyroProcess(gyro):
	# convert from ''
	#bin(int(str(temp), 10))[2:].zfill(8)
	# publish to logger
	
	for i in range(0,len(accel)):
		gyro[i] = bin(int(str(gyro[i]), 10))[2:].zfill(8)	# This converts decimal numbers to binary
	
	print str(lineno()),accel
	gyro = [i+j for i,j in zip(gyro[::2],gyro[1::2])]	# This concatenates every two list items (since each value is broken into two eights
	print str(lineno()),gyro, type(gyro)
	
	for i in range(0,len(gyro)):
		
		if gyro[i].startswith('1'):
			gyro[i] = int(gyro[i],2) ^ int('0xffff',16)
			print str(lineno()),gyro[i],type([i])
			gyro[i] = -(gyro[i]+1)
			print str(lineno()),gyrol[i],type(gyro[i])
		
		else:
			gyro[i] = int(gyro[i],2)
			print str(lineno()),gyro[i],type(gyro[i])
	
	return gyro

def tempProcess(temp):
	# convert
	#bin(int(str(temp), 10))[2:].zfill(8)
	# publish to logger
	
	for i in range( 0, len(accel) ):
		accel[i] = bin( int( str( accel[i] ), 10 ) )[2:].zfill(8)	# This converts decimal numbers to binary
	
	print str(lineno()),accel
	accel = [ i + j for i,j in zip( accel[::2], accel[1:: ] ) ]	# This concatenates every two list items (since each value is broken into two eights
	print str( lineno() ), accel, type(accel)
	
	for i in range(0,len(accel)):
		
		if accel[i].startswith('1'):
			accel[i] = int( accel[i], 2 ) ^ int( '0xffff', 16 )
			print str( lineno() ), accel[i], type( accel[i] )
			accel[i] = - ( accel[i] + 1 )
			print str( lineno() ), accel[i], type( accel[i] )
		
		else:
			accel[i] = int( accel[i], 2 )
			print str( lineno() ), accel[i], type( accel[i] )
	
	return temp

class Handler:
	def __init__(self, stdout = False, fileout = None, autostart = True):
		# Setup zmq publishing client
		context = zmq.Context()
		self.txevents = context.socket(zmq.PUB)
		#self.txevents.connect("tcp://127.0.0.1::12000")
		
		# Let's get it started (clock_set, full-scale gyro range, full-scale accell range, sleep mode off
		initSeq()

		self.stdout = stdout
		if autostart:
			self.IMUListener()

	def IMUListener(self):
		beg = 59
		end = 72
#       Registers 59 - 64: accelerometer; 59&60 = X, 61&62 = Y, 63&64 = Z; 16-bit, two's-compliment
#       65-66: temperature measurement
#       67-72: Gyroscope measurement
		list_size = end - beg
		# Needs to be set to continuous data-measurement mode 

		compare = [None for k in range(0,list_size)]
		t = True
		while t ==True:
			t = False
			#print str(lineno())+": Address: "+ str(address)+" and in hex: "+str(hex(i))
			try:
				#print lineno()
				a = [None for k in range(0,list_size)]
				for j in range(0,list_size):
					adj = j + beg	# adjust the look value to match the registers I'm looking to use
  					temp = i2c.readU8(adj) #i2c.read_byte_data(MPU_ADDR,adj)
					a[j] = temp#bin(int(str(temp), 10))[2:].zfill(8)
				print a
				final[0],final[1],final[2] = accelProcess(a[0:6])
				final[3],final[4],final[5] = tempProcess(a[6:8])
				final[6],final[7],final[8] = gyroProcess(a[8:14])
				if cmp(a,compare) != 0: # I don't want it to write out repeatedly if the value is the same.
					out_file = open("i2coutput.txt","a")
					out_file.write(str(time.ctime())+str(a)+"\n")
					out_file.close()
					#print lineno()
					compare = a
			except IOError, err:
				print str(err)+" "+str(hex(i))
			#time.sleep(1)
if __name__ == "__main__":
	imu = Handler(stdout = True, autostart = True)
