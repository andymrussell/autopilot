# This GPS script is for use with the Adafruit Ultimate GPS module

# Libraries
import Adafruit_BBIO.UART as UART
import sys,signal,serial,inspect,os,time,zmq,pynmea2,SocketServer, json
import smtplib

# Set up global variables

# Commands to change GPS update rate from one to 10 times a second
PMTK_SET_NMEA_UPDATE_1HZ = "$PMTK220,1000*1F\r\n"
PMTK_SET_NMEA_UPDATE_5HZ = "$PMTK220,200*2C\r\n"
PMTK_SET_NMEA_UPDATE_10HZ = "$PMTK220,100*2F\r\n"

# Set the baud rate for the serial
PMTK_SET_BAUD_300 = "$PMTK251,300*2B\r\n"
PMTK_SET_BAUD_1200 = "$PMTK251,1200*1B\r\n"
PMTK_SET_BAUD_2400 = "$PMTK251,2400*1E\r\n"
PMTK_SET_BAUD_4800 = "$PMTK251,4800*14\r\n"
PMTK_SET_BAUD_9600 = "$PMTK251,9600*17\r\n"
PMTK_SET_BAUD_14400 = "$PMTK251,14400*29\r\n"
PMTK_SET_BAUD_19200 = "$PMTK251,19200*22\r\n"
PMTK_SET_BAUD_28800 = "$PMTK251,28800*2A\r\n"
PMTK_SET_BAUD_38400 = "$PMTK251,38400*27\r\n"
PMTK_SET_BAUD_57600 = "$PMTK251,57600*2C\r\n"
PMTK_SET_BAUD_115200 = "$PMTK251,115200*1F\r\n"

# Set LOCUS globals
PMTK_LOCUS_STARTLOG = "$PMTK185,0*22\r\n"
PMTK_LOCUS_LOGSTARTED = "$PMTK001,185,3*3C\r\n"
PMTK_LOCUS_QUERY_STATUS = "$PMTK183*38\r\n"
PMTK_LOCUS_ERASE_FLASH = "$PMTK184,1*22\r\n"

# turn on only the second sentence (GPRMC)
PMTK_SET_NMEA_OUTPUT_RMCONLY = "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"
# turn on GPRMC and GGA
PMTK_SET_NMEA_OUTPUT_RMCGGA = "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"
# turn on ALL THE DATA
PMTK_SET_NMEA_OUTPUT_ALLDATA = "$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"
# turn off output
PMTK_SET_NMEA_OUTPUT_OFF = "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"

# standby command & boot successful message
PMTK_STANDBY = "$PMTK161,0*28\r\n"
PMTK_STANDBY_SUCCESS = "$PMTK001,161,3*36\r\n"  # Not needed currently
PMTK_AWAKE = "$PMTK010,002*2D\r\n"

# Get the release and version
PMTK_Q_RELEASE = "$PMTK605*31\r\n"

# Get updates on antenna status
PGCMD_ANTENNA = "$PGCMD,33,1*6C\r\n"
PGCMD_NOANTENNA = "$PGCMD,33,0*6C\r\n"

#!!!!!!!!!!!!!!!!!!!!!!!#
#    Set up email       #
#!!!!!!!!!!!!!!!!!!!!!!!#

fromaddr = 'at.autopilot@gmail.com'
toaddrs = {'tsandidge@live.com','andymrussell777@gmail.com'}
msg = 'GPS mail server working. '

# Credentials
username = 'at.autopilot'
passwd = 'gps ap sp 2014'

# Start the server; borrowed from Mark Sanborn of nixtutor.com
mailserver = smtplib.SMTP('smtp.gmail.com:587')
mailserver.starttls()
#mailserver.login(username,passwd)
#msg = msg + str(time.ctime())
#mailserver.sendmail(fromaddr,toaddrs,msg)


# Make sure that the driver is 'installed'
#if os.system("cat /sys/devices/bone_capemgr.9/slots | grep 'ADAFRUIT-UART1'") is None:
os.system("echo ADAFRUIT-UART1 > /sys/devices/bone_capemgr.9/slots")

#!!!!!!!!!!!!!!!!!!!!!!!#
#    Setup connections  #
#!!!!!!!!!!!!!!!!!!!!!!!#

# Setup zmq client
context = zmq.Context()
tx_gps = context.socket(zmq.PUB)
tx_gps.connect("tcp://127.0.0.1:12000")

ser = serial.Serial(port = "/dev/ttyO1", baudrate = 9600, timeout=3) # timeout=0 is non-blocking mode (asynchronous I/O)
baud = [300,1200,2400,4800,9600,14400,19200,28800,38400,57600,115200]
print "Optional baud rates:"
print baud 
if ser.isOpen():
    print "UART1 is up and ready! Woot!"
else:
	print "Serial is not open!"
	sys.exit(1)

#!!!!!!!!!!!!!!!!!!!!!!!#
#    Initialize GPS     #
#!!!!!!!!!!!!!!!!!!!!!!!#

ser.flushInput()
ser.flushOutput()

# I want to read the RMC and GGA sentences
ser.write(PMTK_SET_NMEA_OUTPUT_RMCGGA)
print ser.readline()
# Set the update/refresh rate -- Looks like this setting is not working
ser.write(PMTK_SET_NMEA_UPDATE_10HZ)
print ser.readline()
# Set to receive updates from the antenna
ser.write(PGCMD_ANTENNA)
print ser.readline()

# Take note about the ACK packet
# $PMTK001,220,2*31
# where 220 is the command, 0 is invalid, 
# 1 is unsupported, 2 is valid but action failed
# and 3 is valid and success




# Signal trap for Ctrl+c (SIGINT)
def signal_handler(signal, frame): 
	print("Time to go!")
	mailserver.quit()
	ser.close()
	print("Kuht fa osun!")
	sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# This will return the current line number of your program file; works GREAT for debugging
def lineno():
	return inspect.currentframe().f_back.f_lineno

#def checksum(string): # This is supposed to XOR all the bytes except the dollar sign
#	char = 0
#	while(string):
#		char = char^(string++)
#	return str("$"+string+"*"+char)

# This is the big daddy function, doing all the work
def gpsPackager(raw_data,type):
	#context = zmq.Context()

	# Receive on
	#tx_gps = context.socket(zmq.PUB)
	#tx_gps.connect("tcp://172.20.16.153:12000")
	print str(lineno())+"Here is the big function"	
	if raw_data is '0':
		tx_gps.send("log gps bb | Err: Nothing to send")
		return
	else:
		tx_gps.send("log gps bb " + raw_data)
	
	if raw_data.startswith("$GPRMC"): # Recommended minimum specifications for GPS/transit data
#		print str(lineno()) + ": %s section." % str(type)
		nmea = pynmea2.parse(raw_data)
		
		if nmea.data_validity != "A":                                                                	
    		# These packets contain a status field, where A indicates 'Active' and V indicates 'Void'

			pack = {"lat"		: "0",
					"lon"		: "0",
					"gs" 		: "0",
					"hdg"		: "0",
					"magdiff"	: "0"}
			pack["warn"] = "Navigation receiver warning"
	
		else:
			print str(lineno()) +  ": latitude = " + nmea.lat + " and longitude = " + nmea.lon
			print "We had a lock on %s " % str(time.ctime())
			if count == 0:
				mailserver.login(username,passwd) # May need to move this eventually, or not use it at all. Used for 'debugging'
				msg = "GPS has finally had a lock at %s." % str(time.ctime())
				lon = float(nmea.lon[:nmea.lon.find(".")-2]) + float(nmea.lon[nmea.lon.find(".")-2:])/60

			pack = {"lat"		: ("%.6f" % lat) + nmea.lat_dir,
					"lon"		: ("%.6f" % lon) + nmea.lon_dir,
					"gs" 		: nmea.spd_over_grnd,
					"hdg"		: nmea.true_course,
					"magdiff"	: nmea.mag_variation + nmea.mag_var_dir}
			
		tx_gps.send("gps pos " + json.dumps(pack))
		print str(lineno()) +  ": json pack is " + str(pack)
	
	elif raw_data.startswith("$GPGGA"):
		nmea = pynmea2.parse(raw_data)
#		print str(lineno()) + ": %s section at %s." % (str(type), str(time.ctime()))	
	
		pack = {"time"		: nmea.timestamp.isoformat(),# When empty, can't conver Nonetype to isoformat!
				"sig_qual"	: nmea.gps_qual,
				"num_sats"	: nmea.num_sats,
				"alt"		: nmea.altitude,
				"diff_age"	: nmea.age_gps_data}
		
		tx_gps.send("gps info " + json.dumps(pack))

		print str(lineno()) + ": json pack is " + str(pack)
	elif raw_data.startswith("$GPGSA"):
#		print str(lineno()) + ": %s section." % str(type) 
		nmea = pynmea2.parse(raw_data)
		
		pack = {"mode_fix_type"	: nmea.mode_fix_type # 1=no fix, 2=2D, 3=3D
				}
		
		tx_gps.send("gps fix " + json.dumps(pack))

#	try:
#		os.system("echo ADAFRUIT-UART1 > /sys/devices/bone_capemgr.9/slots")
#		ser = serial.Serial(port = "/dev/ttyO1", baudrate = 115200, timeout=10)
#		if ser.isOpen():
#    	    print "UART1 is up and ready! Woot!"
#		else:                                   	
#			print "Serial is not open!"         	
#			sys.exit(1)                         	
#		while True:
#			line_read = ser.readline()
#			gpsPackager(line_read)
#	except:
#		pass
#	finally:
#		print "Cleaning up..."
#		ser.close()
#		print "Kut fa osun!"
#		exit()

count = 0
while True: # Let it rain
	while True:
		i = 9600
		print str(lineno())+": Begin serial baud rate check for %i baud." % i
		ser.baudrate = i
		print "Number of chars waiting in receive buffer: %s." % str(ser.inWaiting())
		try:
			line_read = ser.readline()#(ser.inWaiting())
			#print line_read
			#time.sleep(.5)
			
			if line_read.startswith("$GPGGA"):
				gpsPackager(line_read,line_read[:6])
			elif line_read.startswith("$GPRMC"):
				gpsPackager(line_read,line_read[:6])

		except Exception as exce:
			print str(lineno()) + ": %s." % str(exce)

#if __name__ == "__main__":
#	line_read = ser.readline()
#	gpsPackager(line_read)
