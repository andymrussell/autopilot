from Adafruit_I2C import Adafruit_I2C
import sys, signal, os, inspect
import json, time, math, smbus, zmq

# Device address
GPS_ADDR = 0x1E
# Register addresses

try:
	i2c = Adafruit_I2C(GPS_ADDR) 
except IOError, err:
	print str(lineno()) + ": "+str(err)

def writeBit(addr, index, value): # This function will be used to write singular bits to addresses (effectively)
	curr_byte = bin(int(str(i2c.readU8(addr)), 10))[2:].zfill(8)
	mask = 1 << index
	if value == 0:
		new_byte = int(curr_byte) & ~(mask)
	elif value == 1:
		print str(lineno())+' '+curr_byte
		new_byte = bin(curr_byte) | mask
	else:
		print "Err %s: Bit value must be 1 or 0!" % str(lineno())
	i2c.write8(addr, new_byte)
# TODO: Create a bitCheck function to use instead of if statements. Much more basic, and useful
def initSeq(): # This is essentially a port over from the FreeIMU code;
	# Most of this is calling from ~/libraries/MPU60X0.cpp
	pwr_mgnt_status = bin(int(str(i2c.readU8(0x6B)), 10))[2:].zfill(8) # This takes data from register 0x6B (base 10), and converts it to binary
	if str(pwr_mgnt_status).endswith('0'): # TODO: This should check all three bits, because we want only the one on.
		writeBit(MPU6050_PWR_MGNT_1,XGYRO_BIT,1)
		pwr_mgnt_status = bin(int(str(i2c.readU8(0x6B)), 10))[2:].zfill(8)
		print "Power management status after clock_sel: "+str(pwr_mgnt_status)
	else:
		print "Clock is set appropriately!"

	# Shut off sleep mode;
	pwr_mgnt_status = bin(int(str(i2c.readU8(0x6B)), 10))[2:].zfill(8) # This takes data from register 0x6B (base 10), and converts it to binary
	if str(pwr_mgnt_status).startswith("01"): # Checks to see if IMU (technically MPU6050) is awake
		writeBit(MPU6050_PWR_MGNT_1,SLEEP_BIT,0)
		pwr_mgnt_status = bin(int(str(i2c.readU8(0x6B)), 10))[2:].zfill(8) # This takes data from register 0x6B (base 10), and converts it to binary
		print "Power management status after wakeup: "+str(pwr_mgnt_status)
	else:
		print "We are awake!"
def signal_handler(signal, frame): # Capture things like SIGINT (Ctrl+c)
	print("Time to go!")
	print("Kuht fa osun!")
	sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

# This will return the current line number of your program file
def lineno():
	return inspect.currentframe().f_back.f_lineno

class Handler:
	def __init__(self, stdout = False, fileout = None, autostart = True):
		# Setup zmq publishing client
		context = zmq.Context()
		self.txevents = context.socket(zmq.PUB)
		#self.txevents.connect("tcp://127.0.0.1::12000")
		
		# Let's get it started (clock_set, full-scale gyro range, full-scale accell range, sleep mode off
		#initSeq()

		self.stdout = stdout
		if autostart:
			self.IMUListener()

	def IMUListener(self):
        # These variables are used to search a heap of registers
		beg = 1
		end = 255
		list_size = end - beg

		compare = [None for k in range(0,list_size)]
		while True:
			#print str(lineno())+": Address: "+ str(address)+" and in hex: "+str(hex(i))
			try:
				#print lineno()
				a = [None for k in range(0,list_size)]
				for j in range(0,list_size):
					adj = j + beg	# adjust the look value to match the registers I'm looking to use
  					temp = i2c.readU8(adj) #i2c.read_byte_data(MPU_ADDR,adj)
					a[j] = bin(int(str(temp), 10))[2:].zfill(8)
                    a[j] = str(a[j]) + str(adj)
				if cmp(a,compare) != 0: # I don't want it to write out repeatedly if the value is the same.
					out_file = open("i2coutput.txt","a")
					out_file.write(str(time.ctime())+str(a)+"\n")
					out_file.close()
					#print lineno()
					compare = a
			except IOError, err:
				print str(err)+" "+str(hex(i))
			#time.sleep(1)
if __name__ == "__main__":
	imu = Handler(stdout = True, autostart = True)
