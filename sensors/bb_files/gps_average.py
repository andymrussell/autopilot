import zmq
import math, json, time, pynmea2
import sys, signal, inspect, os

def signal_handler(signal, frame): # Capture things like SIGINT (Ctrl+c)
	print("Time to go!")
	print("Kuht fa osun!")
	sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)
# This will return the current line number of your program file
def lineno():
	return inspect.currentframe().f_back.f_lineno
class Handler:
	def __init__(self, stdout = False, fileout = None, autostart = True):
		context = zmq.Context()
		# Subscribe to GPS events
		self.rxevents = context.socket(zmq.SUB)
		self.rxevents.connect("tcp://127.0.0.1:12001")
		self.rxevents.setsockopt(zmq.SUBSCRIBE, "gps")
		# Publish average
		self.txevents = context.socket(zmq.PUB)
		self.txevents.connect("tcp://127.0.0.1:12000")
		
		self.stdout = stdout
		if autostart:
			self.GPSAv()

	def GPSAv(self):
		time_all,lat_total, lon_total = '0','0','0'
		while True:
			data,start = self.rxevents.recv(),time.time()
			if data.startswith("gps pos"):
				pack = json.loads(data[data.find("{"):]) # Optimized to begin with dictionary
				if start == time_all: # Within one second
					print str(lineno()) + ": debug mode."
					lat_total += pack["lat"]
					lon_total += pack["lon"]
					lat_av = lat_total / count
					lon_av = lon_total / count
				else:
					#print str(lineno()) + ": debug mode."
					if (start != time_all) and (lat_total != '0') and (lon_total != '0'): # One second has elapsed
						print str(lineno()) + ": debug mode."
						package = {"lat_av"	: lat_av,
								   "lon_av"	: lon_av}
						self.txevents.send("gps pos average " + json.dumps(package))
					else: # Fresh start
						#print str(lineno()) + ": debug mode."
						lat_total = pack["lat"]
						lon_total = pack["lon"]
					count = 1
					time_all = start
	
if __name__ == "__main__":
	ave = Handler(stdout = True, autostart = True)
