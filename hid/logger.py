import zmq
#import autopilot
import time

class Logger:
	def __init__(self, stdout = False, fileout = None, autostart = True):
		context = zmq.Context()
		self.rxevents = context.socket(zmq.SUB)
		self.rxevents.connect("tcp://127.0.0.1:12001")
		self.rxevents.setsockopt(zmq.SUBSCRIBE, "")
		self.stdout = stdout
		self.fileout = open(fileout,"a+") if fileout != None else None
		if autostart:
			self.listener()
		
	def listener(self):
		start = time.time()
		if self.fileout != None:
			self.fileout.write("################################# New session ##################################")
		while True:
			data = self.rxevents.recv()
			data = "[%.4f] %s" % (time.time() - start, data)
			if self.stdout:
				print(data)
			if self.fileout != None:
				self.fileout.write(data)
				
if __name__ == "__main__":
	log = Logger(stdout = True, autostart = True)
