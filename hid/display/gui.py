from driver import SSD1322,Drawing
import time
import sys
import math
import cairo
import json
import zmq
from threading import Thread

eventhost = "tcp://127.0.0.1"
rxhost = eventhost + ":12001"
txhost = eventhost + ":12000"

class Autopilot_GUI:
	text = {}

	settings = {} # Actual, in-use settings
	settings_staging = {} # workaround because it's hard to do fast, deep dictionary comparisons
	old_settings_staging = {} # workaround because it's hard to do fast, deep dictionary comparisons
	temp_settings = {"apstate": False, "setting_id": 0, "page_id": "home"} # Don't save these to a file
	old_temp_settings = {} # Keep a bit of state around
	
	available_settings = ["Altitude","Heading","Climb Rate"]
	available_setting_id = ["altitude","heading","climb_rate"]
	
	closing = False

	def __init__(self):
		self.read_settings_file()
		
		self.led = SSD1322(rows = 64, cols = 256)
		self.led.begin()

		self.drawing_agent = Thread(target=self.draw_thread)
		self.drawing_agent.start()
	
	def read_settings_file(self):
		f = open("hid/display/gui.config")
		s = f.read()
		f.close()
		self.settings = json.loads(s)
	
	def write_settings_file(self):
		s = json.dumps(self.settings)
		f = open("hid/display/gui.config","w+")
		f.write(s)
		f.close()

	def new_page(self):
		self.d = Drawing(256,64)
		self.d.ctx.scale (1,1)
		self.d.ctx.translate(0.5,0.5)

	def write_text(self):
		self.d.ctx.select_font_face("Helvetica", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
		self.d.ctx.set_font_size(27)
		self.d.ctx.set_source_rgba(1,1,1,1)
		
		x_bearing, y_bearing, width, height, x_advance, y_advance = self.d.ctx.text_extents(self.text["center"])
		self.d.ctx.move_to(0-x_bearing,41)
		self.d.ctx.show_text(self.text["center"])
		
		self.d.ctx.select_font_face("courier", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
		self.d.ctx.set_font_size(15)

		x_bearing, y_bearing, width, height, x_advance, y_advance = self.d.ctx.text_extents(self.text["top left"])
		self.d.ctx.move_to(0-x_bearing,-y_bearing-1)
		self.d.ctx.show_text(self.text["top left"])

		x_bearing, y_bearing, width, height, x_advance, y_advance = self.d.ctx.text_extents(self.text["bottom left"])
		self.d.ctx.move_to(0-x_bearing,63)
		self.d.ctx.show_text(self.text["bottom left"])

		x_bearing, y_bearing, width, height, x_advance, y_advance = self.d.ctx.text_extents(self.text["top right"])
		self.d.ctx.move_to(255-x_bearing-width,-y_bearing-1)
		self.d.ctx.show_text(self.text["top right"])

		x_bearing, y_bearing, width, height, x_advance, y_advance = self.d.ctx.text_extents(self.text["bottom right"])
		self.d.ctx.move_to(255-x_bearing-width,63)
		self.d.ctx.show_text(self.text["bottom right"])
		
	def draw_thread(self):
		print("Starting draw thread")
		while not self.closing:
			if self.temp_settings != self.old_temp_settings or self.settings_staging != self.old_settings_staging:
				self.old_temp_settings = dict(self.temp_settings)
				self.old_settings_staging = dict(self.settings_staging)
				#print("Drawing page \"" + self.temp_settings["page_id"] + "\"")
				if self.temp_settings["page_id"] == "home":
					self.page_home()
				elif self.temp_settings["page_id"] == "setting_select":
					self.page_setting_select()
				elif self.temp_settings["page_id"] == "setting_altitude":
					self.page_setting_altitude()
				elif self.temp_settings["page_id"] == "setting_heading":
					self.page_setting_heading()
				elif self.temp_settings["page_id"] == "setting_climb_rate":
					self.page_setting_climb_rate()
				elif self.temp_settings["page_id"] == "verify_state":
					self.page_verify_state()
				else:
					self.page_home()
			time.sleep(0)
		print("Stopping draw thread")
	    
	def page_home(self):
		self.new_page()
		
		self.d.ctx.select_font_face("Helvetica", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
		self.d.ctx.set_font_size(27)
		
		if self.temp_settings["apstate"]:
			self.d.ctx.set_source_rgba(1,1,1,1)
			self.d.ctx.rectangle(215,17,255-215,64-2*17)
			self.d.ctx.set_operator(cairo.OPERATOR_XOR)
			self.d.ctx.fill()
			self.d.ctx.move_to(216,42)
			self.d.ctx.show_text("AP")

			self.d.ctx.set_operator(cairo.OPERATOR_OVER)
		else:
			self.d.ctx.move_to(216,42)
			self.d.ctx.set_source_rgba(1,1,1,0.15)
			self.d.ctx.show_text("AP")
			#self.d.ctx.set_line_width(1)
			#self.d.ctx.stroke()
		
		self.d.ctx.set_source_rgba(1,1,1,1)
			
		self.d.ctx.rectangle(215,17,255-215,64-2*17)
		self.d.ctx.set_line_width(1)
		self.d.ctx.stroke()
		
		self.text["top left"]     = u"AP DISABLE" if self.temp_settings["apstate"] else u"AP ENABLE"
		self.text["bottom left"]  = u"SETTINGS"
		self.text["top right"]    = u"enabled" if self.temp_settings["apstate"] else u"disabled"
		self.text["bottom right"] = u"\u00b1" + str(self.settings["climb_rate"]) + "FPM"
		self.text["center"]       = str(self.settings["altitude"]) + u"\u00b4 @ " + str(self.settings["heading"]) + u"\u00b0"
		self.write_text()
		self.display()
	    
	def page_verify_state(self):
		self.new_page()
		self.text["top left"]     = u"AP DISABLE" if self.temp_settings["apstate"] else u"AP ENABLE"
		self.text["bottom left"]  = u"CANCEL"
		self.text["top right"]    = str(self.settings["altitude"]) + u"\u00b4 @ " + str(self.settings["heading"]) + u"\u00b0"
		self.text["bottom right"] = u"\u00b1" + str(self.settings["climb_rate"]) + "FPM"
		self.text["center"]       = u"Really disable AP?" if self.temp_settings["apstate"] else u"Really enable AP?"
		self.write_text()
		self.display()
		
	def page_setting_select(self):
		self.new_page()
		self.text["top left"]     = u"APPLY"
		self.text["bottom left"]  = u"CANCEL"
		self.text["top right"]    = str(self.temp_settings["settings"]["altitude"]) + u"\u00b4 @ " + str(self.temp_settings["settings"]["heading"]) + u"\u00b0"
		self.text["bottom right"] = u"\u00b1" + str(self.temp_settings["settings"]["climb_rate"]) + "FPM"
		self.text["center"]       = self.available_settings[self.temp_settings["setting_id"]]
		self.write_text()
		self.display()
		
	def page_setting_altitude(self):
		self.new_page()
		self.text["top left"]     = u"APPLY"
		self.text["bottom left"]  = u"CANCEL"
		self.text["top right"]    = str(self.settings_staging["altitude"]) + u"\u00b4 @ " + str(self.settings_staging["heading"]) + u"\u00b0"
		self.text["bottom right"] = u"\u00b1" + str(self.settings_staging["climb_rate"]) + "FPM"
		self.text["center"]       = "ALT: " + str(self.settings_staging["altitude"]) + " ft"
		self.write_text()
		self.display()
		
	def page_setting_heading(self):
		self.new_page()
		self.text["top left"]     = u"APPLY"
		self.text["bottom left"]  = u"CANCEL"
		self.text["top right"]    = str(self.settings_staging["altitude"]) + u"\u00b4 @ " + str(self.settings_staging["heading"]) + u"\u00b0"
		self.text["bottom right"] = u"\u00b1" + str(self.settings_staging["climb_rate"]) + "FPM"
		self.text["center"]       = "HDG: " + str(self.settings_staging["heading"]) + u"\u00b0"
		self.write_text()
		self.display()
		
	def page_setting_climb_rate(self):
		self.new_page()
		self.text["top left"]     = u"APPLY"
		self.text["bottom left"]  = u"CANCEL"
		self.text["top right"]    = str(self.settings_staging["altitude"]) + u"\u00b4 @ " + str(self.settings_staging["heading"]) + u"\u00b0"
		self.text["bottom right"] = u"\u00b1" + str(self.settings_staging["climb_rate"]) + "FPM"
		self.text["center"]       = "FPM: " + u"\u00b1" + str(self.settings_staging["climb_rate"])
		self.write_text()
		self.display()

	def display(self):
		self.led.display(self.d)
		
def gui():
	try:
		# Connect to the event manager
		context = zmq.Context()
		
		print("Connecting to event host")
		txevents = context.socket(zmq.PUB)
		txevents.connect(txhost)
		rxevents = context.socket(zmq.SUB)
		rxevents.connect(rxhost)
		rxevents.setsockopt(zmq.SUBSCRIBE, "button")
		rxevents.setsockopt(zmq.SUBSCRIBE, "rotenc")
	except:
		print("Connection to the event host failed!")
		print(sys.exc_info())
		return
	
	try:
		# Initialize the display rendering engine
		print("Initializing GUI and OLED driver")
		ui = Autopilot_GUI()
	except:
		print("GUI module failed to start!")
		print(sys.exc_info())
		return
	
	try:
		while True:
			source, source_id, data = rxevents.recv().split(" ",2)
			
			if ui.temp_settings["page_id"] == "setting_select":
				if source == "button" and data == "down":
					if source_id == "P8_16":
						ui.settings = dict(ui.temp_settings["settings"])
						if "settings" in ui.temp_settings:
							del ui.temp_settings["settings"]
						ui.write_settings_file()
						txevents.send("ui oled " + json.dumps(dict(ui.settings.items() + ui.temp_settings.items())))
						ui.temp_settings["page_id"] = "home"
					elif source_id == "P8_18":
						if "settings" in ui.temp_settings:
							del ui.temp_settings["settings"]
						ui.temp_settings["page_id"] = "home"
					elif source_id == "P8_11":
						ui.settings_staging = dict(ui.temp_settings["settings"])
						ui.temp_settings["page_id"] = "setting_" + ui.available_setting_id[ui.temp_settings["setting_id"]]
				elif source == "rotenc":
					if data == "right":
						ui.temp_settings["setting_id"] = (ui.temp_settings["setting_id"] + 1) % len(ui.available_settings)
					elif data == "left":
						ui.temp_settings["setting_id"] = (ui.temp_settings["setting_id"] - 1) % len(ui.available_settings)
			
			elif ui.temp_settings["page_id"] == "setting_altitude":
				if source == "button" and data == "down":
					if source_id == "P8_16" or source_id == "P8_11":
						ui.temp_settings["settings"] = dict(ui.settings_staging)
						ui.temp_settings["page_id"] = "setting_select"
					elif source_id == "P8_18":
						ui.temp_settings["page_id"] = "setting_select"
				elif source == "rotenc":
					if data == "right":
						ui.settings_staging["altitude"] += 500 if ui.settings_staging["altitude"] < 20000 else 0
					elif data == "left":
						ui.settings_staging["altitude"] -= 500 if ui.settings_staging["altitude"] > 0 else 0
			
			elif ui.temp_settings["page_id"] == "setting_heading":
				if source == "button" and data == "down":
					if source_id == "P8_16" or source_id == "P8_11":
						ui.temp_settings["settings"] = dict(ui.settings_staging)
						ui.temp_settings["page_id"] = "setting_select"
					elif source_id == "P8_18":
						ui.temp_settings["page_id"] = "setting_select"
				elif source == "rotenc":
					if data == "right":
						ui.settings_staging["heading"] = (ui.settings_staging["heading"] + 1) % 360
					elif data == "left":
						ui.settings_staging["heading"] = (ui.settings_staging["heading"] - 1) % 360
			
			elif ui.temp_settings["page_id"] == "setting_climb_rate":
				if source == "button" and data == "down":
					if source_id == "P8_16" or source_id == "P8_11":
						ui.temp_settings["settings"] = dict(ui.settings_staging)
						ui.temp_settings["page_id"] = "setting_select"
					elif source_id == "P8_18":
						ui.temp_settings["page_id"] = "setting_select"
				elif source == "rotenc":
					if data == "right":
						ui.settings_staging["climb_rate"] += 50 if ui.settings_staging["climb_rate"] < 2000 else 0
					elif data == "left":
						ui.settings_staging["climb_rate"] -= 50 if ui.settings_staging["climb_rate"] > 0 else 0
			
			elif ui.temp_settings["page_id"] == "home":
				if source == "button" and data == "down":
					if source_id == "P8_16":
						ui.temp_settings["page_id"] = "verify_state"
					elif source_id == "P8_18":
						ui.temp_settings["settings"] = ui.settings
						ui.temp_settings["page_id"] = "setting_select"
			
			elif ui.temp_settings["page_id"] == "verify_state":
				if source == "button" and data == "down":
					if source_id == "P8_16":
						ui.temp_settings["apstate"] = not ui.temp_settings["apstate"]
						txevents.send("ui oled " + json.dumps(dict(ui.settings.items() + ui.temp_settings.items())))
						ui.temp_settings["page_id"] = "home"
					elif source_id == "P8_18":
						ui.temp_settings["page_id"] = "home"
			
			else:
				ui.temp_settings["page_id"] = "home"
	except:
		ui.closing = True
		print("GUI module shut down")
		print(sys.exc_info())
		return
		
if __name__ == "__main__":
	gui()
