#----------------------------------------------------------------------
# ssd1322.py
# ported by Andrew Russell, Walla Walla University
#
# This library works with 
#   EastRising ER-OLEDM032-1 Series 256x64px OLED display module
# The datasheet for the SSD1322 is available
#   http://www.buydisplay.com/download/ic/SSD1322.pdf
#
# Some important things to know about this device and SPI:
#
# - The SPI interface has no MISO connection.  It is write-only.
#
# - The spidev xfer and xfer2 calls overwrite the output buffer
#   with the bytes read back in during the SPI transfer.
#   Use writebytes instead of xfer to avoid having your buffer overwritten.
#
# - The D/C (Data/Command) line is used to distinguish data writes
#   and command writes - HIGH for data, LOW for commands.  Any attribute 
#   bytes following a command are considered data and must have the D/C
#   line set LOW.  This is not the same as the SSD1306, and the methods
#   in this ported library reflect this difference.
#
# SPI and GPIO calls are made through an abstraction library that calls
# the appropriate library for the platform.
# For the RaspberryPi:
#	 wiring2
#	 spidev
# For the BeagleBone Black:
#	 Adafruit_BBIO.SPI 
#	 Adafruit_BBIO.GPIO
#
# - The pin connections between the BeagleBone Black SPI0 and OLED module are:
#
#	  BBB	-> SSD1322
#	  ------	-------
#	  P9_17  -> CS
#	  P9_15  -> RST   (arbirary GPIO, change at will)
#	  P9_13  -> D/C   (arbirary GPIO, change at will)
#	  P9_22  -> CLK
#	  P9_18  -> DATA
#	  P9_3   -> VIN
#	  N/C	-> 3.3Vo
#	  P9_1   -> GND
#----------------------------------------------------------------------

import Adafruit_BBIO.GPIO as GPIO
from Adafruit_BBIO.SPI import SPI
import cairo
import time
import sys

class SSD1322:

	# Class constants are externally accessible as gaugette.ssd1322.SSD1322.CONST
	# or my_instance.CONST

	ENABLE_GRAY_SCALE	= 0x00
	SET_COLUMN_ADDRESS	= 0x15
	WRITE_RAM_COMMAND	= 0x5c
	READ_RAM_COMMAND	= 0x5d # not used
	SET_ROW_ADDRESS		= 0x75
	SET_MODE			= 0xa0
	SET_START_LINE		= 0xa1
	SET_DISPLAY_OFFSET	= 0xa2
	DISPLAY_OFF			= 0xa4
	DISPLAY_ON			= 0xa5
	DISPLAY_NORMAL		= 0xa6
	DISPLAY_INVERT		= 0xa7
	PARTIAL_DISPLAY_ON	= 0xa8
	PARTIAL_DISPLAY_OFF = 0xa9
	VDD_SELECT			= 0xab
	SLEEP_ON			= 0xae
	SLEEP_OFF			= 0xaf
	SET_PHASE_LENGTH	= 0xb1
	SET_DIVIDER_FREQ	= 0xb3
	UNKNOWN_CMD			= 0xb4 # found in demo code, reduces squeal
	SET_GPIO			= 0xb5
	SET_SECOND_PERIOD	= 0xb6
	ARB_GRAY_TABLE		= 0xb8
	LIN_GRAY_TABLE		= 0xb9
	SET_PRE_VOLTAGE		= 0xbb
	SET_VCOMM			= 0xbe
	SET_CONTRAST		= 0xc1
	MASTER_CONTRAST		= 0xc7
	SET_MUX_RATIO		= 0xca
	LOCK				= 0xfd

	# Device name will be /dev/spidev-{bus}.{device}
	# dc_pin is the data/commmand pin.  This line is HIGH for data, LOW for command.
	# We will keep d/c low and bump it high only for commands with data
	# reset is normally HIGH, and pulled LOW to reset the display

	def __init__(self, bus=0, device=0, dc_pin="P9_16", reset_pin="P9_15", rows=64, cols=256):
		self.cols = cols
		self.rows = rows
		self.dc_pin = dc_pin
		self.reset_pin = reset_pin
		self.spi = SPI(bus, device)
		GPIO.setup(self.reset_pin, GPIO.OUT)
		GPIO.output(self.reset_pin, GPIO.HIGH)
		GPIO.setup(self.dc_pin, GPIO.OUT)
		GPIO.output(self.dc_pin, GPIO.LOW)

	def reset(self):
		GPIO.output(self.reset_pin, GPIO.LOW)
		time.sleep(0.010) # 10ms
		GPIO.output(self.reset_pin, GPIO.HIGH)
		time.sleep(0.2) # 200ms

	def flatten(self,lst):
		o = []
		for x in lst:
			o += x if isinstance(x, list) else [x]
		return o

	def command(self, command, *bytes):
		GPIO.output(self.dc_pin, GPIO.LOW)
		self.spi.writebytes([command])
		bytes = self.flatten(bytes)
		if(len(bytes) > 0):
			GPIO.output(self.dc_pin, GPIO.HIGH)
			start = 0
			remaining = len(bytes)
			max_xfer = 1023
			while remaining > 0:
				count = remaining if remaining <= max_xfer else max_xfer
				remaining -= count
				self.spi.writebytes(bytes[start:start+count])
				start += count

	def begin(self):
		time.sleep(0.001) # 1ms
		self.reset()
		self.command(self.LOCK)
		self.command(self.LOCK, 0x12)
		self.command(self.SLEEP_ON)
		self.command(self.SET_DIVIDER_FREQ, 0x91)
		self.command(self.SET_MUX_RATIO, 0x3f)
		self.command(self.SET_DISPLAY_OFFSET, 0x00)
		self.command(self.SET_START_LINE, 0x00)
		self.command(self.SET_MODE, 0x14, 0x11)
		self.command(self.VDD_SELECT, 0x01)
		self.command(self.UNKNOWN_CMD, 0xa0, 0xfd)
		self.command(self.SET_CONTRAST, 0x80)
		self.command(self.MASTER_CONTRAST, 0x0f)
		self.command(self.SET_PHASE_LENGTH, 0xe2)
		self.command(self.SET_PRE_VOLTAGE, 0x1f)
		self.command(self.SET_SECOND_PERIOD, 0x08)
		self.command(self.SET_VCOMM, 0x07)
		self.command(self.DISPLAY_NORMAL)

		self.command(self.SET_COLUMN_ADDRESS, 0x1c, 0x5b)
		self.command(self.SET_ROW_ADDRESS, 0x00, 0x3f)
		self.command(self.WRITE_RAM_COMMAND, [0x00] * (128*64))

		self.command(self.SLEEP_OFF)

	def clear_display(self):
		self.bitmap.clear()

	def invert_display(self):
		self.command(self.DISPLAY_INVERT)

	def normal_display(self):
		self.command(self.DISPLAY_NORMAL)

	def set_contrast(self, contrast=0x80):
		self.command(self.SET_CONTRAST, contrast)

	def display(self, bitmap):
		self.command(self.SET_COLUMN_ADDRESS, 0x1c, 0x5b)
		self.command(self.SET_ROW_ADDRESS, 0x00, 0x3f)
		self.command(self.WRITE_RAM_COMMAND, bitmap.render())

class Drawing:
	def __init__(self, width, height):
		self.surface = cairo.ImageSurface(cairo.FORMAT_A8, width, height)
		self.ctx = cairo.Context(self.surface)
		self.width = width
		self.height = height

	def view(self,data):
		rows = [data[i:i+self.width/2] for i in xrange(0,len(data),self.width/2)]
		for i in xrange(len(rows)):
			s = ""
			for grp in xrange(len(rows[i])):
				a = rows[i][grp]
				s += "*" if a & 0xf0 > 0 else " "
				s += "*" if a & 0x0f > 0 else " "
			print("|" + s + "|")

	def render(self):
		data = map(ord, self.surface.get_data())
		return [(data[i] & 0xf0) + (data[i+1] >> 4) for i in xrange(0,len(data),2)]
