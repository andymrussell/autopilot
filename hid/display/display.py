import json
import zmq
import serial

eventhost = "tcp://127.0.0.1:12001"
displayhost = "/dev/ttyACM0"

context = zmq.Context()
 
# Socket to forward messages on
print("Setting up event listener")
events = context.socket(zmq.SUB)
events.connect(eventhost)
events.setsockopt(zmq.SUBSCRIBE, "button")
events.setsockopt(zmq.SUBSCRIBE, "rotenc")

# Setup serial connection
print("Listening on", displayhost);
ser = serial.Serial(port = displayhost, baudrate = 115200)

# Button mapping
button = {
    "P8_16": "btn 0",
    "P8_18": "btn 1",
    "P8_11": "btn 2"
}

# Rotary encoder mapping
rotenc = {
    "right": "btn 3",
    "left": "btn 4"
}

# Event queue
queue = []

print("Beginning loop")
while True:
    # Decode data
    data = events.recv().split(" ", 2)
    data = { "source": data[0], "location": data[1], "data": data[2] }
    if(data["source"] == "button" and data["data"] == "down"):
        ser.write(button[data["location"]])
    elif(data["source"] == "rotenc"):
        ser.write(rotenc[data["data"]])
    print data