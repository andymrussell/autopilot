import zmq
import random
import time
 
context = zmq.Context()
 
# Socket with direct access to the sink: used to syncronize start of batch
sink = context.socket(zmq.PUSH)
sink.connect("tcp://localhost:12000")
 
# Initialize random number generator
random.seed()
 
while True:
    data = "left" if random.randint(0,2) else "right"
    sink.send("rotenc 44 26 %s" % data)
    time.sleep(1)