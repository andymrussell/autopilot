import sys
import time
import zmq
 
context = zmq.Context()
 
# Socket to receive messages on
receiver = context.socket(zmq.PULL)
receiver.bind("tcp://*:12000")
 
while True:
    data = receiver.recv().split(" ", 2)
    data = { "source": data[0], "location": data[1], "data": data[2].strip() }
    print data