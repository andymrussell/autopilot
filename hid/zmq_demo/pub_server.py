import zmq
import random
import sys
import time

port = "12000"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

while True:
    dir = "left" if random.randrange(0,2) else "right"
    data = "rotenc 44 26 %s" % dir
    print data
    socket.send(data)
    time.sleep(0.1)
