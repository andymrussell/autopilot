import Adafruit_BBIO.GPIO as GPIO
import time
import zmq
import sys

def button(pin):
	context = zmq.Context()
	
	# Socket to receive messages on
	push = context.socket(zmq.PUB)
	push.connect("tcp://127.0.0.1:12000")
	
	lastval = 0
	lasttime = time.time()
	
	def button_handler(pin):
		global push, lastval, lasttime
		curval = GPIO.input(pin)
		curtime = time.time()
		if(curval != lastval and lasttime + 0.02 < curtime):
			push.send("button %s %s" % (pin, "down" if curval else "up"))
			lastval = curval
			lasttime = curtime

	try:
		#print "Setup IO."
		GPIO.setup(pin,GPIO.IN)
		last = GPIO.input(pin)
		#print "Setup event handler."
		while True:
			GPIO.wait_for_edge(pin, GPIO.BOTH)
			curval = GPIO.input(pin)
			curtime = time.time()
			if(curval != lastval and lasttime + 0.02 < curtime):
				push.send("button %s %s" % (pin, "down" if curval else "up"))
				lastval = curval
				lasttime = curtime
	except:
		pass
	finally:
		print "Cleaning up..."
		GPIO.cleanup()
		print "Bye!"
		exit()

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "Example: %s P8_12" % sys.argv[0]
		exit()
	pin = sys.argv[1]
	button(pin)