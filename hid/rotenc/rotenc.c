// rotenc.c
//
// Manages a rotary encoder on GPIO pins 44 and 26 on a Beaglebone Black.
// Connects to a ZeroMQ server and pushes events to it.
//
// Performance:
// Consumes 5% of the processor with reasonable consistency.  This could
// probably be reduced by getting rid of my polling and exchange it with epoll.
//
// Future features:
// - Stop polling.  Seriously.  Use epoll or something.
// - More advanced decoding routine.  Currently it uses edges of one of the
// channels, and I think it should listen to and wait for both channels.  My
// guess is that the user won't feel a difference, though.

#define SAMPLEPERIOD 1250 // time between channel A samples
#define SAMPLEDELAY 50 // time after channel A that channel B is sampled
#define TXEVENTS "tcp://127.0.0.1:12000"

#define MESSAGELEFT "left"
#define MESSAGERIGHT "right"

#include <stddef.h>
#include <stdio.h>
#include <zmq.h>

#include "../lib/common.h"
#include "../lib/event_gpio.h"

int pins[2];
int debug = 0;

void rotenc_gpio_setup() {
	gpio_export(pins[0]);
	gpio_export(pins[1]);

	gpio_set_direction(pins[0],INPUT);
	gpio_set_direction(pins[1],INPUT);

	gpio_set_value(pins[0],PUD_OFF);
	gpio_set_value(pins[1],PUD_OFF);
}

int rotenc_pin_get_val(int gpio) {
	unsigned int val;
	gpio_get_value(gpio, &val);
	return val;
}

int main(int argc, char **argv) {
	if(argc < 3) {
		printf("Example:\n%s P8_12 P8_14\n", argv[0]);
		return 0;
	}
	else {
		pins[0] = lookup_gpio_by_key(argv[1]);
		pins[1] = lookup_gpio_by_key(argv[2]);
	}

	printf("Init GPIO\n");
	rotenc_gpio_setup();
	
	printf("Init IPC\n");
	void* context = zmq_ctx_new();
	void* pub = zmq_socket(context, ZMQ_PUB);
	zmq_connect(pub, TXEVENTS);

	printf("Listen on GPIO pins %i and %i\n", pins[0], pins[1]);
	int olda = rotenc_pin_get_val(pins[0]);
	int newa, dir = 0;
	while(1) {
		int newa = rotenc_pin_get_val(pins[0]);
		if(newa != olda) {
			usleep(SAMPLEDELAY);
			int b = rotenc_pin_get_val(pins[1]);
			dir = newa ^ b;
			char buf[40];
			snprintf(buf, sizeof buf, "rotenc %s,%s %s", argv[1], argv[2], (dir == 0 ? MESSAGELEFT : MESSAGERIGHT));
			//printf("%s\n",buf);
			zmq_send(pub, buf, strchr(buf,'\0') - buf, 0);
		}
		usleep(SAMPLEPERIOD);
		olda = newa;
		pthread_yield();
	}
}
