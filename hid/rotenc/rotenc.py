import Adafruit_BBIO.GPIO as GPIO
import time

rot1 = "P8_12"
rot2 = "P8_14"

laststep = ""
count = 0

def rotenc_handler(pin):
	global laststep, count
	if laststep == "":
		laststep = pin
	elif pin == rot1 and laststep == rot2:
		laststep = ""
		count += 1
	elif pin == rot2 and laststep == rot1:
		laststep = ""
		count += -1

try:
	GPIO.setup(rot1,GPIO.IN)
	GPIO.setup(rot2,GPIO.IN)
	
	GPIO.add_event_detect(rot1, GPIO.BOTH, callback=rotenc_handler)
	GPIO.add_event_detect(rot2, GPIO.BOTH, callback=rotenc_handler)

	while True:
		print count
		time.sleep(1)
except:
	print "Exception!"
finally:
	print "Cleaning up..."
	GPIO.cleanup()
	print "Bye!"
	exit()
