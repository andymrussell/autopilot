##
## Code copied from http://learning-0mq-with-pyzmq.readthedocs.org/en/latest/pyzmq/devices/forwarder.html
##
import zmq

def event_manager():
	frontend = None
	backend = None
	context = None
	
	try:
		#print("Initializing network communication")
		context = zmq.Context(1)
		frontend = context.socket(zmq.SUB)
		#print(" rx...")
		frontend.bind("tcp://*:12000")
		frontend.setsockopt(zmq.SUBSCRIBE, "")
		#print(" tx...")
		backend = context.socket(zmq.PUB)
		backend.bind("tcp://*:12001")
		#print(" forwarder...")
		zmq.device(zmq.FORWARDER, frontend, backend)
	except Exception, e:
		print(e)
		print("Taking down event host")
	finally:
		if frontend:
			frontend.close()
		if backend:
			backend.close()
		if context:
			context.term()

if __name__ == "__main__":
	event_manager()