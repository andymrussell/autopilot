import usb.core, usb.util
import zmq, sys, os, signal, time

#!!! Important !!!#
# It may be that the BBB does not load the USB driver on boot
# unless it detects a USB-mounted device. If that is the case,
# this will not work. For robustness, a function loading the
# driver manually should be implemented.

class backLog:
	def __init__(self, stdout = False, fileout = None, autostart = True):
		context = zmq.Context()
		
		# TODO: Consider splitting into different threads here
		# Set sockets and filter for each level
		# Debug level: record everything
		self.rxevents = context.socket(zmq.SUB)
		self.rxevents.setsockopt(zmq.SUBSCRIBE, '')
		sub_debug.connect("tcp://127.0.0.1:12001")
		
		# Open a file as None for the moment
		self.fileout = open(fileout,"a+") if fileout != None else None
		if autostart:
			self.listen()
	
	# Capture things like SIGINT (Ctrl+c) 
	def signal_handler(signal, frame):
		print("Time to go!") 
		print("Kuht fa osun!")
		self.fileout.close()
		sys.exit(0) 
	signal.signal(signal.SIGINT, signal_handler)

	#TODO: I need to set it to output to the different files, and then should be done. 

	def listen(self):
		start = time.time()

		if self.fileout != None:
			out_file.write("\n#!!!!  New session on %s  !!!!#\n", % str(start.ctime()))
		
		while True:
			event = self.rxevents.recv()
			event = "[%.4f] %s" % (time.time() - start, event)
			if self.stdout:
				print(event)
			if self.fileout != None:
				self.fileout.write(data)

if __name__ == "__main__":
	log_book = backLog(stdout = False, autostart = True, fileout = "/mnt/ap_log/debug_log.txt")
